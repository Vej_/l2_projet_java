package ulco.cardGame.client;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Client {


    public static void main(String[] args) {
        Socket socket;

        // Current use full variables
        try {
            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();

            String username;
            Object ans;

            Scanner scanner = new Scanner(System.in);
            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Quel est votre pseudo?");
            username= scanner.nextLine();
            oos.writeObject( username );

            Game gamestate= null;
            Board board= null;

            do {
                ObjectInputStream ios= new ObjectInputStream( socket.getInputStream() );
                ans= ios.readObject();

                if( ans instanceof String ) {
                    switch ((String) ans) {
                        case "_RWINNER":
                            if( gamestate.getName().equals( "Bataille" ) ) {
                                ObjectInputStream getter = new ObjectInputStream(socket.getInputStream());
                                System.out.println((String) getter.readObject() + " gagne ce round..");
                            } else {
                                ObjectInputStream winnerget= new ObjectInputStream( socket.getInputStream() );
                                List<String> winners= (List<String>)winnerget.readObject();
                                winners.forEach(System.out::println);
                            }
                        break;

                        case "_LOST":
                            System.out.println( "Vous avez perdu!" );
                        break;

                        case "_DUPE":
                            ObjectOutputStream Reply = new ObjectOutputStream(socket.getOutputStream());

                            System.out.println( "Pseudo dupliqué!\n" );
                            System.out.println( "Entrez un pseudo différent:" );

                            username= scanner.nextLine();
                            oos.writeObject( username );
                        break;


                        case "_MAX":
                            System.out.println( "Le nombre maximale de joueur à déja été atteint!" );
                        case "_END":
                            System.out.println( "Vous avez été déconnecté" );
                            socket.close();
                        break;


                        case "_GWINNER":
                            ObjectInputStream winnerget= new ObjectInputStream( socket.getInputStream() );
                            System.out.println( (String)winnerget.readObject() + " gagne la partie.." );
                        break;

                        case "_CLEARBOARD":
                            board= null;
                        break;

                        case "_AUTH":
                            System.out.println( "Vous avez été authentifié!\n" );
                        break;

                        case "_PLAYED":
                            ObjectInputStream listget= new ObjectInputStream( socket.getInputStream() );
                            List<String> Messages= (List<String>)listget.readObject();

                            Messages.forEach(System.out::println);
                        break;

                        case "_BADCHALLENGE":
                            System.out.println( "Il y a eu une erreur, ré-éssayer de jouer.." );
                        break;

                        case "_PLAY":
                            try {
                                ObjectInputStream getComponents= new ObjectInputStream(socket.getInputStream());
                                List<Component> cards= (List<Component>)getComponents.readObject();
                                ObjectOutputStream Play= new ObjectOutputStream(socket.getOutputStream() );


                                if (gamestate instanceof CardGame) {
                                    System.out.println( "Entrez <any> pour commencer a jouer.." );

                                    if( scanner.hasNextLine() ) {
                                        Play.writeObject( cards.get( 0 ) );
                                    }

                                } else if( gamestate instanceof PokerGame ) {
                                    getComponents= new ObjectInputStream(socket.getInputStream());
                                    List<Component> coins= (List<Component>)getComponents.readObject();

                                    StringBuilder B= new StringBuilder( "Vos cartes:\n" );
                                    for( Component C : cards ) {
                                        B.append( "\t- " +C.getName()+"\n" );
                                    }

                                    B.append( "Vos jetons:" );
                                    for( Component C : coins ) {
                                        B.append("\t- " + C.getName() + "\n");
                                    }
                                    System.out.println( B );

                                    System.out.println( "Quel jetons voulez vous jouez?" );
                                    Play.writeObject( scanner.nextLine() );
                                }

                            } catch ( Exception E ) {
                                E.printStackTrace();
                            }
                        break;

                        case "_AWAIT":
                            System.out.println( "En attente des autres joueurs" );
                        break;

                        default:
                            System.out.printf( "ERR: le serveur a retournée une réponse non supporté! ( Type: String ): %s\n", ans );
                    }
                } else if( ans instanceof Game ) {
                    gamestate= (Game)ans;
                    System.out.printf( "Type de jeu: %s\n", gamestate.getName() );
                    gamestate.displayState();
                } else if( ans instanceof Board ) {
                    board= (Board)ans;
                    board.displayState();
                }
            } while ( !ans.equals( "_END" ) || !ans.equals( "_MAX" ) );
        } catch ( SocketException ex ) {
            System.out.println( ex.getMessage() );
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.games.players.PokerPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.games.players.CardPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.run();
    }

    private void run() {

        try {
            String gamename= "";
            Scanner scan= new Scanner( System.in );
            System.out.println( "Quel jeu voulez vous jouer? Poker ou Bataille" );
            while( !gamename.equals( "Poker" ) && !gamename.equals( "Bataille" ) ) {
                gamename= scan.nextLine();
            }


            // Need to specify your current cardGame.txt file
            switch( gamename ) {
                case "Poker":
                    game = new PokerGame("Poker", 2, 3, "resources/games/pokerGame.txt" );
                break;

                case "Bataille":
                    game = new CardGame("Bataille", 3, "resources/games/cardGame.txt");
                break;
            }

            // Game Loop
            System.out.println("Waiting for new player for " + game.toString());

            playerSockets= new HashMap<>();

            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance
            do {
                Socket socket = server.accept();
                String user;
                Player ply;
                ObjectInputStream clientResponse;

                do {
                    clientResponse = new ObjectInputStream(socket.getInputStream());
                    user = (String) clientResponse.readObject();

                    switch( gamename ) {
                        case "Bataille": ply= new CardPlayer(user); break;
                        case "Poker": ply= new PokerPlayer(user); break;

                        default:
                            System.out.println( "defaulted to null for ply, unknown game" );
                            ply= null;
                        break;

                    }

                } while( !game.addPlayer( socket, ply ) );

                // Store player's socket in dictionary (Map)
                playerSockets.put(ply, socket);
                System.out.println(user + " is now connected...");
                // Tell to other players that new player is connected
            } while (!game.isStarted());

            // run the whole game using sockets

            Player gameWinner = game.run(playerSockets);

            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("_END");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }

            game.removePlayers();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

